import './App.css';
import React from 'react';
import{ Parallax } from 'react-parallax';

const image3 = "https://images.unsplash.com/photo-1470219556762-1771e7f9427d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8YnVpbGRpbmd8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60";

function Connection(){
    return(
        <div>
            <h1 className="text-center">This is Connection Page !</h1>
            <div className="container">
          
                <Parallax bgImage={ image3 } strength={400}>
                <div className="row col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-main solution d-flex align-items-center justify-content-center">
                <div class="rectangle2 rectangle2Small d-flex flex-column align-items-center justify-content-center"> 

               <h2 class="notice noticeSmall display-4 pt-5"> Recharge with sollar energy  </h2>

               <div className="d-flex flex-row"> 
               
                   <div className="box d-flex flex-column align-items-center justify-content-center"> <img className="battery1" src="https://i.pinimg.com/originals/05/64/7c/05647c3c2a9db07539fb0cc89cac9a3c.png" width="20" height="20" /> </div> <p className="text-center sst1"> Grid connection of the facilities </p>
               </div>
               <div className="corner align-self-end mt-auto p-2 text-center cornerSmall"><span className="sspan">scroll</span></div>
              </div>
               </div>
                </Parallax>
                <div style ={{height:'100vh'}}></div>
            </div>

        </div>
    )
}
export default Connection;