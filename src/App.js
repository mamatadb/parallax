// import logo from './logo.svg';
import './App.css';
import React from 'react';
import ReactDOM from 'react-dom';
import {Route, BrowserRouter as Router, Switch, Link} from "react-router-dom";
import {Navbar,Nav} from 'react-bootstrap';
import Home from "./Home";
import About from "./About"
import Connection from "./Connection";
import Contact from "./Contact";
import Projects from "./Projects";
import 'bootstrap/dist/css/bootstrap.min.css';




function App() {
 
  return (
    <Router>
    <div className="container"> 
      {/* <nav>
         <ul>
            <li> <Link to="/"> Home </Link></li>
            <li> <Link to="/about">About</Link></li>
            <li> <Link to="/connection">Connection</Link></li>
            <li> <Link to="/projects">Projects</Link></li>
            <li> <Link to="/contact">Contact</Link></li>

        </ul>

      </nav>  */}


  {/* <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
  <a className="navbar-brand" href="#">Brand Name</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNav">
    <ul className="navbar-nav navbar-right">

      <li className="nav-item active"><Link to="/">
        Home <span class="sr-only">(current)</span> </Link>
      </li>

      <li className="nav-item">
      <Link to="/about"> About </Link>
      </li>

      <li className="nav-item">
      <Link to="/connection"> Connection </Link>
      </li>

      <li className="nav-item">
      <Link to="/projects"> Projects </Link>
      </li>

      <li className="nav-item">
      <Link to="/contact"> Contact </Link>
      </li> 

    </ul>
  </div>
</nav> */}



{/* Navbar starts */}


<Navbar collapseOnSelect expand="lg" bg="light" variant="light">
{/* <Navbar collapseOnSelect expand="lg" bg={image1} variant="light">
  <Navbar.Brand href="#home">Sollar</Navbar.Brand> */}
  <Navbar.Brand href="#home"> <Link to="/">
       sollar <span class="sr-only">(current)</span> </Link></Navbar.Brand>
      
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">

    <Nav className="mr-auto">
    {/* <Nav.Link href="#home"> <Link to="/">
        Home <span class="sr-only">(current)</span> </Link></Nav.Link>*/}
        </Nav> 
    <Nav.Link href="#about"> <Link to="/about"> About </Link> </Nav.Link>
    
    <Nav.Link href="#connection"> <Link to="/connection"> Connection </Link></Nav.Link>
      <Nav.Link eventKey={2} href="#contact">  <Link to="/contact"> Contact </Link></Nav.Link>
  {/* <Nav> */}
      <Nav.Link href="#project"><Link to="/projects"> Projects </Link></Nav.Link> 
    {/* </Nav> */}
    {/* <Nav>
      <Nav.Link href="#deets">More deets</Nav.Link>
      <Nav.Link eventKey={2} href="#memes">
        Dank memes
      </Nav.Link>
    </Nav> */}
    
  </Navbar.Collapse>

</Navbar>

{/* Navbar ends */}
    <Switch>
    <Route path= "/" exact component = {Home} />
    <Route path="/about" component = {About} />
    <Route path="/contact" component = {Contact} />
    <Route path="/connection" component = {Connection} />
    <Route path="/projects" component = {Projects} />
    </Switch>
    </div>
    </Router>
  );
}

export default App;
