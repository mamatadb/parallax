import './App.css';
import React from 'react';
import{ Parallax } from 'react-parallax';
import {Navbar,Nav} from 'react-bootstrap';
import {Spring} from 'react-spring/renderprops';

const image2 = "https://images.unsplash.com/photo-1498329741116-4d1987b210d0?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MzB8fHdhdGVyZmFsbHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60";

const inlineStyle = {
    background : 'transparent',
    border:'5px solid black',
    left:'50%',
    top:'50%',
    position:'absolute',
    padding:'40px 40px 80px 40px',
    borderRadius:'20px',
    fontWeight:'bold',
    transform: 'translate(-50%, -50%)',
    fontSize:'3.1em',
    height: '65%',
    width:'60%'
}
const pStyle ={
    left:'32%',
    top:'79%',
    transform: 'translate(-50%, -50%)',
    position:'absolute',
    fontSize:'0.8em',
    paddingTop:'6px 2px 7px 2px'
}
function Projects(){
    return(
    
        <div>
            <Spring 
                from = {{ opacity: 0}}
                to ={{ opacity:1,}}
                config={{ delay:1000, duration:1000}}
            >
                { props => (
                    <div style ={props}> 
                       <div> <svg  className ="bubble1 bub" width="80" height="80" viewBox="0 0 80 80">
  <defs>
    <linearGradient id="linear-gradient" x1="0.5" y1="0.035" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#0878d5" stop-opacity="0.243"/>
      <stop offset="0.106" stop-color="#8ac9f5" stop-opacity="0.204"/>
      <stop offset="0.262" stop-color="#88c8f4" stop-opacity="0"/>
      <stop offset="0.49" stop-color="#84c6f3" stop-opacity="0"/>
      <stop offset="0.601" stop-color="#5ebbf6" stop-opacity="0"/>
      <stop offset="0.839" stop-color="#39b1fa" stop-opacity="0.231"/>
      <stop offset="0.972" stop-color="#02a1ff" stop-opacity="0.365"/>
      <stop offset="1" stop-color="#086393" stop-opacity="0.325"/>
      <stop offset="1" stop-color="#91e4f4" stop-opacity="0.012"/>
    </linearGradient>
  </defs>
  <circle id="Ellipse_10" data-name="Ellipse 10" cx="40" cy="40" r="40" fill="url(#linear-gradient)"/>
</svg>
</div>    
     </div>
          )}
            </Spring>
            <h1 className="text-center">This is Project  Page !</h1>
            <Parallax bgImage={ image2 } strength={400}>
            <div className ="myDiv" style= {{height:500}}>
             
            <div style={inlineStyle} > <span className="mainText mainTextSmall"> Recharge with solar energy</span></div>
       
            <p style={pStyle} className="paraText paraTextSmall"> Grid connection of the facilities </p>
           
       
            </div>
        </Parallax>
         
        <div style ={{height:'100vh'}}></div>
        </div>
        
    )
}
export default Projects;