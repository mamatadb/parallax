import './App.css';
import React, { Fragment } from 'react';
import Button from 'react-bootstrap/Button';
import{ Parallax } from 'react-parallax';
import 'bootstrap/dist/css/bootstrap.min.css';

const image1 = "https://images.unsplash.com/photo-1554435493-93422e8220c8?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8YnVpbGRpbmd8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60";

const inlineStyle = {
    background : 'transparent',
    border:'5px solid black',
    left:'50%',
    top:'50%',
    position:'absolute',
    padding:'80px 40px 100px 40px',
    borderRadius:'20px',
    fontWeight:'bold',
    transform: 'translate(-50%, -50%)',
    fontSize:'3.2em' 
}
const pStyle ={
    left:'45%',
    top:'80%',
    transform: 'translate(-50%, -50%)',
    position:'absolute',
    fontSize:'0.8em',
}
function Home(){
    return(
        <Fragment>

<div>
            {/* <h1>This is Home Page !</h1> */}
            <Parallax bgImage={ image1 } strength={400}>
            <div style= {{height:500}}>
             
            <div style={inlineStyle} className="rect"> <span className="mainText"> Recharge with solar energy</span></div>
            <p style={pStyle} className="grid gridSmall">Grid connection of the facilities</p>
         
            </div>
        </Parallax> 
        </div>
      
{/* <-- section ---> */}
<section className="section section-light"> 
    <h2> Section 1</h2>

    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
</p>
</section>

{/* ---------------next section ---------> */}

<section class="section section-dark">
    <h2> Section 2</h2>
    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
</p>
</section>

{/* next section------------------ */}
<section class="section section-light">
    <h2> Section 3</h2>
    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
</p>
</section>
        
         <Parallax bgImage={ image1 } strength={400}>
            <div style= {{height:500}}>
            <div style={inlineStyle}> Heading Lorem ipsum dolor sit</div>
            </div>
        </Parallax>
        <div style ={{height:'100vh'}}></div>
        </Fragment>
    )
}
export default Home;